import React, { Component, Fragment } from 'react';
import axios from 'axios';
import Search from "../../components/search/Search";
import './Weather.css';
import Day from "../../components/Day/Day";



export default class Weather extends Component {

    state = {
        city: 'Kiev',
        country: 'UA',
        info: [],
    };

    clickedHandler = () => {
        axios
            .get(
                'http://api.openweathermap.org/data/2.5/forecast/daily?q=' +
                this.state.city +
                '&units=metric&APPID=66e9f8945cfe75879c81a124f4d95052',
            )
            .then(resp => {
                const country = resp.data.city.country;
                this.setState({
                    country: country,
                    info: resp.data.list,
                });
                console.log(resp.data);
            });
    };

    changeHandler = event => {
        const city = event.target.value;
        this.setState({ city: city });
    };




    render(){

        let day = this.state.info.map((t, id) => {
            const temp = Math.floor(t.temp.day);
            const dateNumber = t.dt;
            let d = new Date(0);
            d.setUTCSeconds(dateNumber);
            let n = d.getMonth()+1<10 ? 0 : false;
            const date = `${d.getFullYear()}.${n}${d.getMonth()+1}.${d.getDate()}`;
            return  <Day key={id} date={date} temp={temp}/>
        });
        if (day.length===0){
            day = 'value';
        }

        return (
            <Fragment>
                <Search clicked={this.clickedHandler} city={this.state.city} changed={this.changeHandler} />
                <div className="flex flex--a-center flex--j-center">
                    {day}

                </div>
            </Fragment>
        );
    }
}