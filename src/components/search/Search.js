import React, { Fragment } from 'react';


const Search = props => {
  return (
      <Fragment>
          <input type="text" value={props.city} onChange={props.changed} />
          <button onClick={props.clicked}>Search</button>
      </Fragment>
  );
};

export default Search;
