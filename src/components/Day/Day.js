import React from 'react';
import './Day.css'

const Day = props =>{
  return <div className='day'>
      <div>
          <div className="date">{props.date}</div>
          <div className="temp">{props.temp} <sup>o</sup>C</div>
      </div>

  </div>
};
export default Day;
